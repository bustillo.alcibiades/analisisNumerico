#!/usr/bin/env python
# -*- coding: utf-8 -*-

def puntoFijo(g, p0, Tol, N):
	i = 1
	while i <= N:
		p = g(p0)
		if abs(p - p0) < Tol:
			print "El valor de p es {}".format(p)
			return
		i = i + 1
		p0 = p
	print "El método fracaso despues de {} itereaciones.".format(N)

def prueba():
	g = lambda x: x - x ** 3 - 4 * x ** 2 + 10
	g1 = lambda x: 0.5 * (10 - x ** 3) ** 0.5
	p0 = 1.5
	Tol = 1e-9
	N = 30
	puntoFijo(g1, p0, Tol, N)

prueba()