#!/usr/bin/env python
# -*- coding: utf-8 -*-

from math import sin, cos, pi

def posicionFalsa(f, p0, p1, Tol, N):
	i = 2
	q0, q1 = f(p0), f(p1)

	print "{:6} {:3}".format("n", "p")	

	while(i <= N):
		p = p1 - q1 * (p1 - p0)/(q1 - q0)

		print "{:} {:4}".format(i, p)
		
		if abs(p - p1) < Tol:
			print "El valor de p = {}".format(p)
			return 

		i = i + 1
		q = f(p)
		if (q * q1 < 0):
			p0 = p1
			q0 = q1

		p1, q1 = p, q		

	print "El método fracaso despues de {} itereaciones.".format(N)

def prueba():
	f = lambda x: cos(x) - x	
	p0 = 0
	p1 = pi/4
	Tol = 1e-6
	N = 10
	posicionFalsa(f, p0, p1, Tol, N)

prueba()