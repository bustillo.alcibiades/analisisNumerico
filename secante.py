#!/usr/bin/env python
# -*- coding: utf-8 -*-

from math import sin, cos, pi

def secante(f, p0, p1, Tol, N):
	i = 2
	q0, q1 = f(p0), f(p1)

	print "{:6} {:3}".format("n", "p")	

	while(i <= N):
		p = p1 - q1 * (p1 - p0)/(q1 - q0)

		print "{:} {:4}".format(i, p)
		
		if abs(p - p1) < Tol:
			print "El valor de p = {}".format(p)
			return 

		i = i + 1
		p0, q0, p1, q1, = p1, q1, p, f(p)

	print "El método fracaso despues de {} itereaciones.".format(N)

def prueba():
	f = lambda x: cos(x) - x	
	p0 = 0
	p1 = pi/4
	Tol = 1e-6
	N = 10
	secante(f, p0, p1, Tol, N)

prueba()