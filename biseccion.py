#!/usr/bin/env python
# -*- coding: utf-8 -*-


def biseccion(f, a, b, Tol, N):
	i = 1
	FA = f(a)

	print "{:15} {:16} {:16} {:20} {:20}".format("n","ai","bi","pi","f(pi)")
	
	while i <= N:
		p = a + (b - a) / 2
		FP = f(p)
				
		if FP == 0  or (b - a) / 2 < Tol:
			FP = f(p)
			print "El valor de p = {}".format(p)
			return 
		
		print "{} {:16} {:16} {:16} {:23}".format(i,a,b,p,FP)		

		i = i + 1
		 
		if FA * FP > 0:
			 a = p
			 FA = FP
		else:
			b = p
	print "El método fracaso despues de {} itereaciones.".format(N)
			 
def prueba():
	f = lambda x: x ** 3 + 4 * x ** 2 - 10
	a, b = 1.0, 2.0
	Tol = 1e-4
	N = 14
	biseccion(f, a, b, Tol, N)

prueba() 
