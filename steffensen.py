#!/usr/bin/env python
# -*- coding: utf-8 -*-

def steffensen(g, p0, Tol, N):
	i = 1
	print "{:6} {:3}".format("n", "p")	

	while(i <= N):
		p1 = g(p0) 
		p2 = g(p1)
		p = p0 - ((p1 - p0) ** 2)/(p2 - 2 * p1 + p0)

		print "{:} {:4}".format(i, p)
		
		if abs(p - p0) < Tol:
			print "El valor de p = {}".format(p)
			return 

		i = i + 1
		p0 = p 

	print "El método fracaso despues de {} itereaciones.".format(N)

def prueba():
	f = lambda x: (10 / (x + 4)) ** 0.5	
	p0 = 1.5	
	Tol = 1e-6
	N = 10
	steffensen(f, p0, Tol, N)

prueba()