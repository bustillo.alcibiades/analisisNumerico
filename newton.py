#!/usr/bin/env python
# -*- coding: utf-8 -*-
from math import sin, cos, pi

def newton(f, g, p0, Tol, N):
	i = 1
	while i <= N:
		p = p0 - f(p0) / g (p0)
		print "n = {:} p = {:3}".format(i, p)
		if abs(p - p0) < Tol:
			print "El valor de p es {}".format(p)
			return
		i = i + 1
		p0 = p
	print "El método fracaso despues de {} itereaciones.".format(N)


def prueba():
	f = lambda x: cos(x) - x
	g = lambda x: - sin(x) - 1
	p0 = pi/4
	Tol = 1e-10
	N = 30
	newton(f, g, p0, Tol, N)

prueba()